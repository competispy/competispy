from flask import request
from flask_restful import abort, Resource
from marshmallow import Schema
from webargs import fields, validate
import pymongo

from competispy.views.inscriptions.ConversorTiempos import ConversorTiempos
from competispy.db import db_adaptor

inscription_search_args = {
    'event_distance': fields.Int(),
    'event_style': fields.Str(),
    'event_gender': fields.Str(),
    'event_type': fields.Str(),
    'event_category': fields.Str(),
    'participant_name': fields.Str(),
    'participant_license': fields.Str(),
    'participant_born_year_gte': fields.Int(load_from='low'),
    'participant_born_year_lt': fields.Int(load_from='high'),
    'participant_leverade_name': fields.Str(),
    'participant_leverade_surname': fields.Str(),
    'heat': fields.Int(),
    'lane': fields.Int(),
    'session': fields.Int(),
    'club': fields.Str(),
    'inscription_pool_length': fields.Int(),
    'inscription_chronometer_type': fields.Str(),
    'championship_name': fields.Str(required=True),
    'championship_date_gte': fields.DateTime(load_from='low', format='%d/%m/%Y'),
    'championship_date_lt': fields.DateTime(load_from='high', format='%d/%m/%Y'),
    'start_index': fields.Int(missing=0),
    'sort_mode': fields.Str(missing='inscription_time'),  # other possible is heat
    'results_limit': fields.Int(missing=10, validate=[validate.Range(min=1, max=100)]),
}

InscriptionSchema = Schema.from_dict(inscription_search_args)

inscription_entry_args = {
    'event': fields.Nested({
        'distance': fields.Int(required=True),
        'style': fields.Str(required=True),
        'gender': fields.Str(required=True),
        'type': fields.Str(required=True),
        'event_number': fields.Int(required=True),
        'category': fields.Str(required=True),
    }),
    'participant': fields.Nested({
        'name': fields.Str(required=True),
        'license': fields.Str(required=True),
        'born_year': fields.Int()
    }),
    'heat': fields.Int(missing=0),
    'lane': fields.Int(missing=0),
    'session': fields.Int(missing=0),
    'club': fields.Str(required=True),
    'initial_position': fields.Int(missing=9999),
    'inscription_time': fields.Nested({
        'time': fields.Float(required=True),
        'date': fields.DateTime(format='%d/%m/%Y'),
        'location': fields.Str(),
        'pool_length': fields.Int(required=True),
        'chronometer_type': fields.Str(required=True),
    }),
    'championship': fields.Nested({
        'name': fields.Str(required=True),
        'date': fields.DateTime(required=True, format='%d/%m/%Y')
    }),
    'user': fields.Str(required=True),
    'pass': fields.Str(required=True)
}

InscriptionEntrySchema = Schema.from_dict(inscription_entry_args)


def _recurrent_dict_update(original_dict, new_dict_values):
    for key, val in new_dict_values.items():
        if key in original_dict and type(original_dict[key]) is dict and type(val) is dict:
            _recurrent_dict_update(original_dict[key], val)
        else:
            original_dict[key] = val


class Filter:
    def get_filter(self, filter_input):
        search_filter = {}
        for k, v in filter_input.items():
            func = getattr(self, 'get_' + k)
            _recurrent_dict_update(search_filter, func(v))

        sort_mode = search_filter['sort_mode']
        start_index = search_filter['start_index']
        results_limit = search_filter['results_limit']
        del (search_filter['sort_mode'])
        del (search_filter['start_index'])
        del (search_filter['results_limit'])

        return search_filter, sort_mode, start_index, results_limit

    def get_event_distance(self, value):
        return {'event.distance': value}

    def get_event_style(self, value):
        return {'event.style': value}

    def get_event_gender(self, value):
        return {'event.gender': value}

    def get_event_type(self, value):
        return {'event.type': value}

    def get_event_category(self, value):
        return {'event.category': value}

    def get_participant_name(self, value):
        return {'participant.name': {'$regex': value, '$options': 'i'}}

    def get_participant_leverade_name(self, value):
        return {'participant.leverade.name': {'$regex': value, '$options': 'i'}}

    def get_participant_leverade_surname(self, value):
        return {'participant.leverade.surname': {'$regex': value, '$options': 'i'}}

    def get_participant_license(self, value):
        return {'participant.license': value}

    def get_participant_born_year_gte(self, value):
        return {'participant.born_year': {'$gte': value}}

    def get_participant_born_year_lt(self, value):
        return {'participant.born_year': {'$lt': value}}

    def get_heat(self, value):
        return {'heat': value}

    def get_lane(self, value):
        return {'lane': value}

    def get_session(self, value):
        return {'session': value}

    def get_club(self, value):
        return {'club': {'$regex': value, '$options': 'i'}}

    def get_inscription_pool_length(self, value):
        return {'inscription_time.pool_length': value}

    def get_inscription_chronometer_type(self, value):
        return {'inscription_time.chronometer_type': value}

    def get_championship_name(self, value):
        return {'championship.name': value}

    def get_championship_date_gte(self, value):
        return {'championship.date': {'$gte': value}}

    def get_championship_date_lt(self, value):
        return {'championship.date': {'$lt': value}}

    def get_sort_mode(self, value):
        return {'sort_mode': value}

    def get_start_index(self, value):
        return {'start_index': value}

    def get_results_limit(self, value):
        return {'results_limit': value}


class Inscription(Resource):
    def get(self):
        inscriptions_db = db_adaptor.get_inscriptions_db()
        args = InscriptionSchema().load(request.args.to_dict())
        (search_filter, sort_mode, start_index, results_limit) = Filter().get_filter(args)
        print(search_filter)

        db_query_result = inscriptions_db.find(search_filter).limit(results_limit).sort(
            [("event.number", pymongo.DESCENDING), (sort_mode, 1)])
        result = []
        for entry in db_query_result:
            del (entry['_id'])
            ConversorTiempos.convertir_a_tiempo_inscripcion(entry)
            # print(entry)
            result.append(entry)
        return result

    def post(self):
        global adminuser
        global adminpass
        inscriptions_db = db_adaptor.get_inscriptions_db()

        args = InscriptionEntrySchema().load(request.args.to_dict())
        if args['user'] != adminuser or args['pass'] != adminpass:
            abort(400)

        # args['inscription_time'] = timedelta(seconds=args['inscription_time'])

        del (args['user'])
        del (args['pass'])

        db_query_result = inscriptions_db.insert(args)
        # args['inscription_time'] = args['inscription_time'].total_seconds()
        return {}
