from flask_restful import Resource
import pymongo
from bson.son import SON

from competispy.db import db_adaptor


class Championships(Resource):
    def get(self):
        inscriptions_db = db_adaptor.get_inscriptions_db()

        pipeline = [
            {"$group": {"_id": {"championship": "$championship"}}},
            {"$sort": SON([("_id.championship.date", -1)])},
        ]
        pipeline_result = inscriptions_db.aggregate(pipeline)
        if type(pipeline_result) == pymongo.command_cursor.CommandCursor:
            championships = [
                event["_id"]["championship"]["name"] for event in list(pipeline_result)
            ]
        else:
            championships = [
                event["_id"]["championship"]["name"]
                for event in pipeline_result["result"]
            ]
        print(championships)
        return championships


class Championship(Resource):
    def get(self, championship_name):
        inscriptions_db = db_adaptor.get_inscriptions_db()

        db_query_result = inscriptions_db.find(
            {"championship.name": championship_name}
        ).limit(1)
        result = []
        for entry in db_query_result:
            result.append(entry["championship"])
        return result[0] if len(result) != 0 else {}
