from flask_restful import Resource
import pymongo

from competispy.db import db_adaptor


class TeamList(Resource):
    def get(self, championship_name):
        inscriptions_db = db_adaptor.get_inscriptions_db()

        pipeline = [
            {"$match": {"championship.name": championship_name}},
            {"$group": {"_id": {"club": "$club"}}}
        ]
        pipeline_result = inscriptions_db.aggregate(pipeline)
        if type(pipeline_result) == pymongo.command_cursor.CommandCursor:
            clubs = [event['_id']['club'] for event in list(pipeline_result)]
        else:
            clubs = [event['_id']['club'] for event in pipeline_result['result']]
        return clubs


class Team(Resource):
    def get(self, championship_name, team_name):
        inscriptions_db = db_adaptor.get_inscriptions_db()

        pipeline = [
            {"$match": {"championship.name": championship_name,
                        "club": team_name, "participant.born_year":
                            {
                                "$ne": None
                            }}},
            {"$group": {
                "_id": {"participant": "$participant"},
                'gender': {"$first": '$event.gender'}}}
        ]
        pipeline_result = inscriptions_db.aggregate(pipeline)
        if type(pipeline_result) == pymongo.command_cursor.CommandCursor:
            team_members = [{
                'participant': event['_id']['participant'],
                'gender': event['gender']
            } for event in list(pipeline_result)]
        else:
            team_members = [{
                'participant': event['_id']['participant'],
                'gender': event['gender']
            } for event in pipeline_result['result']]
        return team_members
