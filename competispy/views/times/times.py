from flask import request, g
from flask_restful import abort, Resource
from marshmallow import Schema, ValidationError
from webargs import fields, validate
from datetime import datetime

from competispy.db import db_adaptor

from competispy.security.guards import (
    authorization_guard
)

time_search_args = {
    'event_distance': fields.Int(),
    'event_style': fields.Str(),
    'event_pool_length': fields.Int(),
    'event_chronometer_type': fields.Str(),
    'participant_born_year': fields.Int(load_from='low'),
    'participant_name': fields.Str(),
    'participant_surname': fields.Str(),
    'season': fields.Str(missing='all'),
    'sort_mode': fields.Str(missing='time'),  # other possible is date
    'sort_direction': fields.Int(missing=1, validate=[validate.Range(min=0, max=1)]),
    'results_limit': fields.Int(missing=1, validate=[validate.Range(min=1, max=100)]),
    'start_index': fields.Int(missing=0),
}

TimeSchema = Schema.from_dict(time_search_args)

time_entry_args = {
    'event': fields.Nested({
        'distance': fields.Int(required=True),
        'style': fields.Str(required=True),
        'pool_length': fields.Int(required=True),
        'chronometer_type': fields.Str(required=True),
    }),
    'participant': fields.Nested({
        'name': fields.Str(required=True),
        'surname': fields.Str(required=True),
        'born_year': fields.Int(required=True)
    }),
    'when': fields.DateTime(format='%d/%m/%Y', required=False),
    'championship': fields.Str(missing='no championship', required=False),
    'time': fields.Float(required=True),
    'date': fields.DateTime(format='%d/%m/%Y'),
    'season': fields.Str(required=True)
}

TimeEntrySchema = Schema.from_dict(time_entry_args)


def _recurrent_dict_update(original_dict, new_dict_values):
    for key, val in new_dict_values.items():
        if key in original_dict and type(original_dict[key]) is dict and type(val) is dict:
            _recurrent_dict_update(original_dict[key], val)
        else:
            original_dict[key] = val


class Filter:
    def get_filter(self, filter_input):
        search_filter = {}
        for k, v in filter_input.items():
            func = getattr(self, 'get_' + k)
            _recurrent_dict_update(search_filter, func(v))

        sort_mode = search_filter['sort_mode']
        sort_direction = search_filter['sort_direction']
        start_index = search_filter['start_index']
        results_limit = search_filter['results_limit']
        del (search_filter['sort_mode'])
        del (search_filter['sort_direction'])
        del (search_filter['start_index'])
        del (search_filter['results_limit'])

        return search_filter, sort_mode, sort_direction, start_index, results_limit

    def get_event_distance(self, value):
        return {'event.distance': value}

    def get_event_style(self, value):
        return {'event.style': value.upper()}

    def get_event_pool_length(self, value):
        return {'event.pool_length': value}

    def get_event_chronometer_type(self, value):
        return {'event.chronometer_type': value}

    def get_participant_name(self, value):
        return {'participant.name': {'$regex': value, '$options': 'i'}}

    def get_participant_surname(self, value):
        return {'participant.surname': {'$regex': value, '$options': 'i'}}

    def get_participant_born_year(self, value):
        return {'participant.born_year': value}

    def get_time(self, value):
        return {'time': value}

    def get_date(self, value):
        return {'date': value}

    def get_season(self, value):
        return {'season': value} if value != 'all' else {}

    def get_sort_mode(self, value):
        return {'sort_mode': value}

    def get_sort_direction(self, value):
        return {'sort_direction': value}

    def get_start_index(self, value):
        return {'start_index': value}

    def get_results_limit(self, value):
        return {'results_limit': value}


class Time(Resource):
    @authorization_guard
    def get(self):
        try:
            args = TimeSchema().load(request.args.to_dict())
        except ValidationError as err:
            print(err)
            abort(400)

        (search_filter, sort_mode, sort_direction, start_index, results_limit) = Filter().get_filter(args)
        print(search_filter)

        times_db = db_adaptor.get_times_db()
        db_query_result = times_db.find(search_filter).limit(results_limit).sort(
            [(sort_mode, sort_direction)])
        result = []
        for entry in db_query_result:
            del (entry['user'])
            result.append(entry)
        if len(result) == 0:
            abort(404)
        return result

    @authorization_guard
    def post(self):
        try:
            args = TimeEntrySchema().load(request.json)
        except ValidationError as err:
            print(err)
            abort(400)
        times_db = db_adaptor.get_times_db()

        args['user'] = g.access_token['sub']
        args['when'] = datetime.now()

        args['event']['style'] = args['event']['style'].upper()

        print(args)

        db_insertion_result = times_db.update_one(
            self._get_find_existing_time_filter(args),
            {"$set": args},
            upsert=True
        )

        return {}

    def _get_find_existing_time_filter(self, args):
        return {
            'event.distance': args['event']['distance'],
            'event.style': args['event']['style'],
            'event.pool_length': args['event']['pool_length'],
            'event.chronometer_type': args['event']['chronometer_type'],
            'participant.name': args['participant']['name'],
            'participant.surname': args['participant']['surname'],
            'participant.born_year': args['participant']['born_year'],
            'time': args['time'],
            'season': args['season'],
            'date': args['date'],
        }
