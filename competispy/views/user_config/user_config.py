from flask import request, g
from flask_restful import abort, Resource
from marshmallow import Schema, ValidationError
from webargs import fields

from competispy.db import db_adaptor

from competispy.security.guards import (
    authorization_guard
)


user_config_entry_args = {
    'user_id': fields.Str(required=True),
    'name': fields.Str(required=True),
    'surname': fields.Str(required=True),
    'born_year': fields.Int(required=True),
}

UserConfigEntrySchema = Schema.from_dict(user_config_entry_args)


class UserConfig(Resource):
    @authorization_guard
    def get(self):
        user = g.access_token['sub']
        user_config_db = db_adaptor.get_user_config_db()
        user_config = user_config_db.find_one({'user': user})
        return user_config

    @authorization_guard
    def post(self):
        try:
            args = UserConfigEntrySchema().load(request.json)
        except ValidationError as err:
            print(err)
            abort(400)

        user = g.access_token['sub']
        if args['user_id'] != user:
            abort(404)

        user_config_db = db_adaptor.get_user_config_db()
        db_insertion_result = user_config_db.update_one(
            {'user_id': args['user_id']},
            {"$set": args},
            upsert=True
        )
        return {}
