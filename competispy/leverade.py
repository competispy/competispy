import requests


class Leverade:
    STYLE = {'braza': 6, 'espalda': 4, 'estilos': 8, 'libre': 5, 'mariposa': 7}

    @staticmethod
    def get_request(filters, result_size=50, result_include=None, page_number=1):
        payload = dict(sort='value',
                       filter=r'!resultable[license].id:null,style.discipline.id:38,official:true,value>0,{}'.format(
                           ','.join(filters)))
        payload[r'page[number]'] = page_number
        payload[r'page[size]'] = result_size
        if result_include:
            payload[r'include'] = result_include
        r = requests.get(r'https://api.leverade.com/managers/210453/individual_phaseresults',
                         params=Leverade.__transform_payload_to_string(payload))
        return r.json()

    @staticmethod
    def get_request_data(filters, result_size=50, result_include=None, page_number=1):
        return Leverade.get_request(filters, result_size, result_include, page_number)['data']

    @staticmethod
    def find_leverade_participant_profile(name, surname, birth_year):
        response_data = Leverade.get_request([Leverade.get_aproximate_profile_filter(name, surname, birth_year)],
                                             result_size=25, result_include='resultable.profile')
        print(response_data)
        found_profiles = []
        if 'included' in response_data:
            for d in response_data['included']:
                if d['type'] == 'profile':
                    d['birth_year'] = birth_year
                    found_profiles.append(d)
                    # print(d)
        # print('numero de profiles: {}'.format(len(found_profiles)))
        # print('numero de resultados: {}'.format(len(response_data['data'])))
        return found_profiles[0] if len(found_profiles) == 1 else None

    @staticmethod
    def find_leverade_participant_profile_with_name(name, born_year):
        name_parts = name.replace(',', '').split(' ')
        for i in range(1, len(name_parts)):
            surname = ' '.join(name_parts[0:i])
            name = ' '.join(name_parts[i:])
            # print('intentando nombre={}, apellidos={}'.format(name, surname))
            leverade_participant = Leverade.find_leverade_participant_profile(name, surname, born_year)
            if leverade_participant is None:
                continue
            else:
                return leverade_participant

    @staticmethod
    def get_aproximate_profile_filter(first_name, last_name, birth_year):
        return r'profile.first_name^{},profile.last_name^{},profile.birthdate>{},profile.birthdate<{}'.format(
            first_name.replace(' ', '+'), last_name.replace(' ', '+'), Leverade.__get_last_day_prev_year(birth_year),
            Leverade.__get_first_day_next_year(birth_year))

    @staticmethod
    def get_exact_profile_filter(first_name, last_name, birth_year):
        return r'profile.first_name:{},profile.last_name:{},profile.birthdate>{},profile.birthdate<{}'.format(
            first_name.replace(' ', '+'), last_name.replace(' ', '+'), Leverade.__get_last_day_prev_year(birth_year),
            Leverade.__get_first_day_next_year(birth_year))

    @staticmethod
    def get_event_filter(distance, style):
        return r'goal:{},style.id:{}'.format(distance, Leverade.STYLE[style])

    @staticmethod
    def get_pool_filter(pool):
        return r'discipline_fields.pool_size:{}'.format(pool)

    @staticmethod
    def __transform_payload_to_string(payload):
        return "&".join("%s=%s" % (k, v) for k, v in payload.items())

    @staticmethod
    def __get_last_day_prev_year(year):
        return '{}-12-31'.format(year - 1)

    @staticmethod
    def __get_first_day_next_year(year):
        return '{}-01-01'.format(year + 1)


class LeveradeParticipant:
    def __init__(self, leverade_profile):
        self._leverade_profile = leverade_profile
        self._born_year = self._leverade_profile['birth_year']
        self._profile_filter = Leverade.get_exact_profile_filter(self._leverade_profile['attributes']['first_name'],
                                                                 self._leverade_profile['attributes']['last_name'],
                                                                 self._born_year)

    def get_event_results(self, distance, style, pool=None):
        filters = [self._profile_filter, Leverade.get_event_filter(distance, style)]
        if pool is not None:
            filters.append(Leverade.get_pool_filter(pool))
        return Leverade.get_request(filters)


# print(Leverade.get_leverade_data_for_name('julian aguilar miguel angel', 1982))
participant = LeveradeParticipant(Leverade.find_leverade_participant_profile('MMMM', 'MMMM', 9999))
print(participant.get_event_results(50, 'braza', 25))
