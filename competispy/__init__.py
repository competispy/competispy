from bson import ObjectId
from flask import Flask, Blueprint
from flask_restful import abort, Api
from webargs.flaskparser import parser
import os
from datetime import datetime
from flask.json import JSONEncoder
from prometheus_flask_exporter import RESTfulPrometheusMetrics

from competispy import exception_views
from competispy.views.inscriptions.inscriptions import Inscription
from competispy.views.championships.championships import Championships, Championship
from competispy.views.championships.teams import TeamList, Team
from competispy.views.times.times import Time
from competispy.views.user_config.user_config import UserConfig
from competispy.db import db_adaptor
from competispy.security.auth0_service import auth0_service

db_adaptor.setup()


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        # print type(obj)
        try:
            if isinstance(obj, datetime):
                return obj.strftime("%d/%m/%Y")
            elif isinstance(obj, ObjectId):
                return str(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


class MyConfig(object):
    RESTFUL_JSON = {}

    @staticmethod
    def init_app(app):
        app.config["RESTFUL_JSON"]["cls"] = app.json_encoder = CustomJSONEncoder


def create_app():
    auth0_audience = os.environ["AUTH0_AUDIENCE"]
    auth0_domain = os.environ["AUTH0_DOMAIN"]
    auth0_service.initialize(auth0_domain, auth0_audience)

    app = Flask(__name__)
    api_bp = Blueprint("api", __name__, url_prefix="/api/v1")
    app.config.from_object(MyConfig)
    MyConfig.init_app(app)
    api = Api(api_bp)

    api.add_resource(Championships, "/championships")
    api.add_resource(Championship, "/championships/<championship_name>")
    api.add_resource(Inscription, "/inscriptions")
    api.add_resource(TeamList, "/championships/<championship_name>/teams")
    api.add_resource(Team, "/championships/<championship_name>/teams/<team_name>")
    api.add_resource(Time, "/times")
    api.add_resource(UserConfig, "/userconfig")

    app.register_blueprint(api_bp)
    app.register_blueprint(exception_views.bp)

    metrics = RESTfulPrometheusMetrics.for_app_factory()
    metrics.init_app(app, api)

    @app.route("/liveness")
    def liveness():
        return "OK"

    @app.route("/", defaults={"u_path": ""})
    @app.route("/<path:u_path>")
    def index(u_path):
        return app.send_static_file("index.html")

    # This error handler is necessary for usage with Flask-RESTful
    @parser.error_handler
    def handle_request_parsing_error(
        err, req, schema, error_status_code, error_headers
    ):
        """webargs error handler that uses Flask-RESTful's abort function to return
        a JSON error response to the client.
        """
        abort(error_status_code, errors=err.messages)

    return app
