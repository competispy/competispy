# -*- coding: UTF-8 -*-
from __future__ import print_function
import sys

import argparse

from datetime import datetime, timedelta
from DatabaseConnector import get_inscriptions_db

from competitor_parser import CompetitorParserBuilder
from event_parsers import (
    SplashMeetEventSeries,
    SplashMeetEvent,
    SplashMeetEventCtoValencia,
)
from individual_competitor_parsers import (
    SplashMeetParserCompetitor,
    SplashMeetParserCompetitorWithHeat,
    SplashMeetParserCompetitorCtoMadrid,
    SplashMeetParserCompetitorCtoMadridWithHeat,
    SplashMeetParserCompetitorCtoValencia,
    SplashMeetParserCompetitorCtoValenciaWithHeat,
)
from relay_competitor_parsers import (
    SplashMeetParserRelay,
    SplashMeetParserRelayWithHeat,
    SplashMeetParserRelayCtoMadrid,
    SplashMeetParserRelayCtoMadridWithHeat,
    SplashMeetParserRelayCtoValencia,
)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


parser = argparse.ArgumentParser()
parser.add_argument("champ_file", help="file with the inscriptions in txt form")
parser.add_argument("champ_name", help="championship name")
parser.add_argument("champ_date", help="championship date in the form dd/MM/YYYY")
parser.add_argument(
    "champ_type",
    type=str,
    choices=[
        "inscriptions_madrid",
        "heats_madrid",
        "inscriptions_spain",
        "heats_spain",
        "inscriptions_valencia",
        "heats_valencia",
    ],
    help="championship data source type",
)
parser.add_argument(
    "-p", "--pool", type=int, choices=[25, 50], default=25, help="pool length"
)
parser.add_argument(
    "-c",
    "--chrono",
    type=str,
    choices=["E", "M"],
    default="E",
    help="chrono system. [E]lectronic or [M]anual",
)
parser.add_argument(
    "-e", "--erase", action="store_true", help="remove existing championship from db"
)
parser.add_argument("-d", "--db", default="localhost", help="database URL")
parser.add_argument(
    "-m",
    "--db-mode",
    choices=["local", "remote_rest"],
    default="local",
    help="database URL",
)
parser.add_argument("-t", "--test", action="store_true", help="use fake database")
parser.add_argument(
    "--disable-relay", action="store_true", help="disable relay parsing"
)
parser.add_argument(
    "--print-unmatched",
    action="store_true",
    help="make the script to print the not matched lines in stderr",
)

args = parser.parse_args()

inscriptions_db = get_inscriptions_db(mode=args.db_mode, test=args.test, host=args.db)

file = open(args.champ_file)

championship_name = args.champ_name.upper()
championship_date = datetime.strptime(args.champ_date, "%d/%m/%Y")
default_pool_length = args.pool
default_chronometer_type = args.chrono

competitor_parser_builder = (
    CompetitorParserBuilder()
    .championship_name(championship_name)
    .championship_date(championship_date)
    .default_pool_length(default_pool_length)
    .default_chronometer_type(default_chronometer_type)
)

competition_type = args.champ_type

compatition_parser = {
    "inscriptions_madrid": (
        SplashMeetEvent,
        SplashMeetParserCompetitorCtoMadrid,
        SplashMeetParserRelayCtoMadrid,
    ),
    "heats_madrid": (
        SplashMeetEventSeries,
        SplashMeetParserCompetitorCtoMadridWithHeat,
        SplashMeetParserRelayCtoMadridWithHeat,
    ),
    "inscriptions_spain": (
        SplashMeetEvent,
        SplashMeetParserCompetitor,
        SplashMeetParserRelay,
    ),
    "heats_spain": (
        SplashMeetEventSeries,
        SplashMeetParserCompetitorWithHeat,
        SplashMeetParserRelayWithHeat,
    ),
    "inscriptions_valencia": (
        SplashMeetEventCtoValencia,
        SplashMeetParserCompetitorCtoValencia,
        SplashMeetParserRelayCtoValencia,
    ),
    "heats_valencia": (
        SplashMeetEventSeries,
        SplashMeetParserCompetitorCtoValenciaWithHeat,
        SplashMeetParserRelayCtoMadridWithHeat,
    ),
}


(SplashMeetEventClass, SplashMeetParserCompetitorClass, SplashMeetParserRelayClass) = (
    compatition_parser[competition_type]
)

parseadores_pruebas = []
parseadores_pruebas.append(SplashMeetEventClass(relay=False))
parseadores_pruebas[-1].add_competitor_parser(
    competitor_parser_builder.parser_object(SplashMeetParserCompetitorClass()).build()
)
parseadores_pruebas[-1].set_db(inscriptions_db)


if not args.disable_relay:
    parseadores_pruebas.append(SplashMeetEventClass(relay=True))
    parseadores_pruebas[-1].add_competitor_parser(
        competitor_parser_builder.parser_object(SplashMeetParserRelayClass()).build()
    )
    parseadores_pruebas[-1].set_db(inscriptions_db)

parseador_prueba_actual = None

for line in file:
    line = line.rstrip()

    for parserador_prueba in parseadores_pruebas:
        if parserador_prueba.match(line):
            parseador_prueba_actual = parserador_prueba

    if not parseador_prueba_actual:
        continue

    if not parseador_prueba_actual.match_competitor(line) and args.print_unmatched:
        eprint(line)

if args.erase:
    inscriptions_db.delete_many({"championship.name": championship_name})

for parserador_prueba in parseadores_pruebas:
    parserador_prueba.store()
