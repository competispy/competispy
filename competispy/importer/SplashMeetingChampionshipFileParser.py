# -*- coding: UTF-8 -*-
from ChampionshipFileParser import ChampionshipFileParser
import re
from ConversorTiempos import ConversorTiempos
from datetime import datetime
from DatabaseConnector import get_inscriptions_db


class SplashMeetingChampioshipFileParser(ChampionshipFileParser):
    def __init__(self, file_path, championship_data, pool_length, pool_chronometer):
        super(SplashMeetingChampioshipFileParser, self).__init__(file_path)
        self.event_re = re.compile(
            'Prueba (?P<event_number>\d+)\s+(?P<gender>\w+)(\.)?,\s+(?P<relay>\d+ x )?(?P<distance>\d+)m\s+(?P<style>\w+)\s+')
        self.relay_category_re = re.compile('(?P<category>\+\d+)$')
        self.individual_category_re = re.compile('(?P<category>\d+)\+$')
        self.participant_re = re.compile(
            '\s*(?P<initial_position>\d+)\s+(?P<name>(\S+ )+)\s*(?P<year>\d+)(?P<name_2>( )?[A-Z][a-z]+)?\s*(?P<club>[A-Z](\S+ )+)\s*(?P<inscription_time>\:?(\d{1,2}:)?\d{1,2}\.\d{1,2})')
        self.event_data = None
        self.previous_line = ''
        self.championship_data = championship_data
        self.pool_length = pool_length
        self.pool_chronometer = pool_chronometer

    def _process_line(self, new_line):
        match = self.event_re.match(new_line)
        if match:
            match_event_dict = match.groupdict()
            self.event_data = {
                'event_number': int(match_event_dict['event_number']),
                'distance': int(match_event_dict['distance']),
                'style': match_event_dict['style'].upper(),
                'gender': self._get_gender(match_event_dict['gender']),
                'category': None,
                'type': 'individual' if match_event_dict['relay'] is None else 'relay'
            }
            self.previous_line = ''
            return
        elif self.event_data is not None:
            match = self.individual_category_re.match(new_line) if self.event_data['type'] == 'individual'\
                else self.relay_category_re.match(new_line)
            if match:
                self.event_data['category'] = match.group('category')
                self.previous_line = ''
                return
            match = self.participant_re.match(new_line)
            if match is None:
                match = self.participant_re.match(self.previous_line + new_line.strip())
            if match:
                participant_data = {
                    'license': '',
                    'name': (match.group('name').upper().strip() + ' ' +
                             (match.group('name_2').upper() if match.group('name_2') is not None else '').strip()).strip(),
                }
                if self.event_data['type'] == 'individual':
                    participant_data['born_year'] = self._get_year(match.group('year'))
                inscription_time_data = {
                    'time': self.process_inscription_time(
                        match.group('inscription_time').lstrip('0').lstrip(':').strip()
                    ),
                    'pool_length': self.pool_length,
                    'chronometer_type': self.pool_chronometer,
                    'date': datetime.strptime('01/01/1900', '%d/%m/%Y'),
                    'location': ''
                }
                data = {
                    'event': self.event_data,
                    'participant': participant_data,
                    'inscription_time': inscription_time_data,
                    'championship': self.championship_data,
                    'heat': 0,
                    'lane': 0,
                    'session': 0,
                    'club': match.group('club').upper().strip(),
                    'initial_position': int(match.group('initial_position'))
                }
                ConversorTiempos.convertir_a_tiempo_database(data)
                self._set_data(data)
                self.previous_line = ''
                return
        self.previous_line = new_line + ' '

    @staticmethod
    def _get_gender(gender):
        if gender == 'Masc':
            return 'MASCULINO'
        elif gender == 'Fem':
            return 'FEMENINO'
        elif gender == 'Mixto':
            return 'MIXTO'
        else:
            raise ValueError

    @staticmethod
    def _get_year(year):
        year_boundry = datetime.now().year % 100 + 5  # 5 years old minimum
        year = int(year)
        return 1900 + year if year >= year_boundry else 2000 + year


db = get_inscriptions_db(mode='remote_rest')

for i in range(1, 39):
    print('EntryList_%d.txt' % i)
    champParser = SplashMeetingChampioshipFileParser('EntryList_%d.txt' % i,
                                                     {'name': 'XXIV CTO. DE ESPAÑA "OPEN" DE INVIERNO MASTERS',
                                                      'date': datetime.strptime('01/02/2018', '%d/%m/%Y')}, 25, 'E')
    for data in champParser:
        #print data
        db.insert(data)
