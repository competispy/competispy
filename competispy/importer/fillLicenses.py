from DatabaseConnector import get_inscriptions_db
import re

inscriptions_db = get_inscriptions_db()

search_filter = {
  'participant.license': ''
}
db_query_result = inscriptions_db.find(search_filter)

counter = 0
for participant_without_license in db_query_result:
    name_to_search = '"' + ''.join(participant_without_license['participant']['name'].split(',')) + '"'
    inscription_id = participant_without_license['_id']
    print('------------------------------')
    print(name_to_search)
    participant_with_license = inscriptions_db.find_one({"$text": {"$search": name_to_search, "$diacriticSensitive": True}, 'participant.license': re.compile(".+")})
    print(participant_without_license)
    counter += 1
    print('entry without license', inscription_id)
    print(participant_with_license)
    print('------------------------------')
    if participant_with_license != None:
        #participant_without_license['participant']['license'] = participant_with_license['participant']['license']
        print('updating')
        found_license = participant_with_license['participant']['license']
        inscriptions_db.find_one_and_update({'_id': inscription_id}, {'$set': {'participant.license': found_license}})


