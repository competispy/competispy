# -*- coding: UTF-8 -*-
import re
from competitor_parser import CompetitorParser, get_fixed_gender
from common_regex_strings import *


class NadadorParser(CompetitorParser):
    def __init__(self):
        super(NadadorParser, self).__init__(
            '\s*(?P<initial_position>\d+)\s+(?P<license>\S+)\s+(?P<name>(\S+ )+)\s*(?P<year>\d{4})\s+(?P<club>(\S+ )+)\s*(?P<inscription_time>\:?(\d{1,2}:)?\d{1,2}\.\d{1,2})\s+(?P<swimming_pool>\d{2}.{2}[ME])(\s+(?P<inscription_time_date>\d{2}/\d{2}/\d{4})\s+(?P<inscription_time_location>.*))?')


class NadadorFinalParser(CompetitorParser):
    def __init__(self):
        super(NadadorFinalParser, self).__init__(
            '\s*(?P<lane>\d+( [DI])?)\s+(?P<license>\S+)\s+(?P<name>(\S+ )+)\s*(?P<year>\d{4})\s+(?P<club>(\S+ )+)\s*(?P<inscription_time>\:?((\d{1,2}:)*\d{1,2}\.\d{1,2})|9:59:59.59)\s+(?P<swimming_pool>\d{2}..[ME])')
        self.use_update_from_db = True


class SplashMeetParserCompetitorCtoMadrid(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorCtoMadrid, self).__init__(
            '\s*{}\s+{}\s*{}\s+{}?{}\s*(({})|{})\s*{}'.format(initial_position_regex, name_regex, year_2_digits_regex, club_number_regex, club_name_regex, time_with_group_regex, no_time_regex, inscription_time_regex))

class SplashMeetParserCompetitorCtoMadrid2024(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorCtoMadrid2024, self).__init__(
            '\s*{}\s+{}\s*{}\s+{}?{}\s*{}'.format(initial_position_regex, name_regex, year_2_digits_regex, club_number_regex, club_name_regex, inscription_time_regex))


class SplashMeetParserCompetitorCtoMadridWithHeat(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorCtoMadridWithHeat, self).__init__(
            '\s*{}\s+{}\s*{}\s+{}\s+{}?{}\s+{}'.format(lane_regex, name_regex, category_regex, year_2_digits_regex, club_number_regex, club_name_regex, inscription_time_regex))


class SplashMeetParserCompetitor(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitor, self).__init__(
            '\s*{}\s+{}\s*{}?\s*{}\s*{}?{}\s*{}\s*{}'.format(initial_position_regex, name_regex, year_2_digits_regex, category_regex, club_number_regex, club_name_regex, time_with_group_regex, inscription_time_regex))


class SplashMeetParserCompetitorWithHeat(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorWithHeat, self).__init__(
            '\s*{}\s+{}\s*{}\s*{}?\s*{}?{}\s*{}'.format(lane_regex, name_regex, year_2_digits_regex, category_regex, club_number_regex, club_name_regex, time_with_group_regex))


class SplashMeetParserCompetitorCtoValencia(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorCtoValencia, self).__init__(
            '\s*{}\s+{}\s*{}\s+{}?{}{}\s+{}?\s*{}'.format(initial_position_regex, name_regex, year_2_digits_regex, club_number_regex, club_name_regex, inscription_time_no_group_regex, pool_length_regex, inscription_time_regex))


class SplashMeetParserCompetitorCtoValenciaWithHeat(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserCompetitorCtoValenciaWithHeat, self).__init__(
            '\s*{}\s+{}\s*{}\s+{}?{}\s{}'.format(lane_regex, name_regex, year_2_digits_regex, club_number_regex, club_name_regex, inscription_time_regex))
