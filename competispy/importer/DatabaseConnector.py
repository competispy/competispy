import requests
import pymongo
import os

mongo_host = 'db-svc'
mongo_port = '27017'
mongo_user = os.environ['COMPETISPY_MONGO_APPLICATION_USER']
mongo_pass = os.environ['COMPETISPY_MONGO_APPLICATION_PASS']

class TestingDb:
    def insert_one(self, data):
        print('inserting:', data)
        print()

    def remove(self, key):
        pass

    def find_one(self, key):
        return None


class RemoteRestDb:
    def __init__(self, server_url='http://localhost:8080'):
        self.server_url = server_url

    def insert_one(self, data):
        output_data = data.copy()
        output_data['inscription_time']['date'] = str(output_data['inscription_time']['date'])
        output_data['championship']['date'] = str(output_data['championship']['date'])
        output_data['user'] = os.environ['COMPETISPY_ADMIN_USER']
        output_data['pass'] = os.environ['COMPETISPY_ADMIN_PASS']
        r = requests.post('%s/inscriptions' % self.server_url, json=output_data)
        print('response code:', r.status_code, 'response:', r.text)
        return

    def remove(self, key):
        pass

    def find_one(self, key):
        return None


def get_inscriptions_db(mode='local', test=False, host=mongo_host):
    if mode == 'local':
        if test:
            print('test mode')
            return TestingDb()
        else:
            print('db mode')
            client = pymongo.MongoClient(host + ':' + mongo_port, username=mongo_user, password=mongo_pass, authSource="admin", authMechanism='SCRAM-SHA-256')
            db = client.competispy
            return db.inscriptions
    elif mode == 'remote_rest':
        return RemoteRestDb()

