# -*- coding: UTF-8 -*-

lane_regex = '(?P<lane>\d+)'
lane_with_side_regex = '(?P<lane>\d+( [DI])?)'
initial_position_regex = '(?P<initial_position>\d+)'
name_regex = '(?P<name>[^,]+,\s*[^0-9]+)'
year_2_digits_regex = '(?P<year>\d{2})'
category_regex = '(?P<category>\d{2}\+)'
club_number_no_group_regex = '(\w{5} - )'
club_number_regex = '((?P<club_number>\w{5}) -)'
club_regex = '(\s[^\s]+)+'
club_name_regex = '(?P<{}>{})'.format('club', club_regex)
club_name_as_name_regex = '(?P<{}>{})'.format('name', club_regex)
time_regex = '(:?(\d{1,2}:)?\d{2}\.\d{2})'
pool_length_regex = '(\d{2}m)'
official_pool_length_regex = '((?P<pool_length>\d{2})m)'
time_with_group_regex = '(?P<time>{})\s+{}'.format(time_regex, official_pool_length_regex)
pool_type_regex = '[ME]'
no_time_regex = '(NT|9:59:59.59)'
inscription_time_regex = '(?P<inscription_time>{}|{})'.format(time_regex, no_time_regex)
inscription_time_no_group_regex = '({}|{})'.format(time_regex, no_time_regex)
year_regex = '(?P<year>\+\d{2,3})'
license_regex = '(?P<license>\S+)'

