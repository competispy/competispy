from datetime import timedelta


class ChampionshipFileParser(object):
    def __init__(self, file_path):
        self.file_path = file_path
        self.file_descriptor = open(self.file_path, 'r')
        self.is_data_new = False
        self.data = None
        self.db = None

    def __iter__(self):
        return self

    def next(self):
        while not self._is_new_data_available():
            new_line = self.file_descriptor.readline()
            # print new_line.rstrip()
            if new_line != '':
                self._process_line(new_line.rstrip())
            else:
                # print 'stop'
                raise StopIteration()
        self._set_data_dirty()
        return self.data

    def _process_line(self, new_line):
        raise NotImplemented

    def _is_new_data_available(self):
        return self.is_data_new

    def _set_data_dirty(self):
        self.is_data_new = False

    def _set_data(self, data):
        self.data = data
        self.is_data_new = True

    @staticmethod
    def process_inscription_time(inscription_time):
        milliseconds_separator = '.' if '.' in inscription_time else ','
        if ':' in inscription_time:
            minutes_time = int(inscription_time.split(':')[0])
            seconds_time = int(inscription_time.split(':')[1].split(milliseconds_separator)[0])
            milliseconds_time = int(inscription_time.split(':')[1].split(milliseconds_separator)[1]) * 10
        else:
            if milliseconds_separator in inscription_time:
                minutes_time = 0
                seconds_time = inscription_time.split(milliseconds_separator)[0]
                milliseconds_time = inscription_time.split(milliseconds_separator)[1]
                if len(milliseconds_time) == 3:
                    seconds_time = seconds_time + milliseconds_time[0]
                    milliseconds_time = milliseconds_time[1:]
                seconds_time = int(seconds_time)
                milliseconds_time = int(milliseconds_time) * 10
            else:
                minutes_time = 0
                seconds_time = int(inscription_time) / 100
                milliseconds_time = int(inscription_time) % 100
        return timedelta(minutes=minutes_time, seconds=seconds_time,
                         milliseconds=milliseconds_time).total_seconds()
