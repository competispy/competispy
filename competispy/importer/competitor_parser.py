# -*- coding: UTF-8 -*-
import re
from datetime import datetime, timedelta
from ConversorTiempos import ConversorTiempos


def get_fixed_gender(input_gender):
    output_gender = input_gender
    if output_gender == 'MASC':
        output_gender = 'MASCULINO'
    elif output_gender == 'FEM':
        output_gender = 'FEMENINO'
    elif output_gender == 'MIXTOED':
        output_gender = 'MIXTO'
    return output_gender


class CompetitorParser(object):
    def __init__(self, regular_expression):
        print(regular_expression)
        print("\n-------\n")
        self.regular_expression = re.compile(regular_expression)
        self.prueba_parser = None
        self.championship_name = 'a champ'.upper()
        self.championship_date = datetime.strptime("16/11/2000", "%d/%m/%Y")
        self.default_pool_length = 25
        self.default_chronometer_type = 'E'
        self.is_relay = False

    def specific_match(self, match_competitor, inscription_data):
        self.update_inscription_time_data(inscription_data, match_competitor)

    @staticmethod
    def update_category(inscription_data):
        inscription_data_keys = inscription_data.keys()
        for needed_key in ['participant', 'championship', 'event']:
            if not needed_key in inscription_data_keys:
                return
        if not 'born_year' in inscription_data['participant'] or not 'date' in inscription_data['championship']:
            return
        inscription_data['event']['category'] = CompetitorParser.get_category_from_born_year(inscription_data['participant']['born_year'], inscription_data['championship']['date'])

    @staticmethod
    def update_leverade_data(match_competitor, inscription_data):
        name_match = match_competitor.group('name')
        if not name_match:
            return
        name_surname = name_match.strip('*').split(', ')
        if len(name_surname) != 2:
            return
        inscription_data['participant']['leverade'] = {}
        inscription_data['participant']['leverade']['surname'] = name_surname[0].strip()
        inscription_data['participant']['leverade']['name'] = name_surname[1].strip()


    @staticmethod
    def update_lane(match_competitor, inscription_data):
        try:
            inscription_data['lane'] = int(match_competitor.group('lane'))
        except IndexError:
            pass


    def match(self, line):
        match_competitor = self.regular_expression.match(line)
        if match_competitor and self.prueba_parser:
            inscription_data = self.get_inscription_data(match_competitor)
            inscription_data['championship'] = self.get_championship_data(match_competitor)
            inscription_data['event'] = self.get_event_data(match_competitor)
            inscription_data['participant'] = self.get_participant_data(match_competitor)
            inscription_data['inscription_time'] = self.get_inscription_time(match_competitor)

            ConversorTiempos.convertir_a_tiempo_database(inscription_data)

            self.specific_match(match_competitor, inscription_data)
            CompetitorParser.update_lane(match_competitor, inscription_data)
            if self.is_relay:
                if self.prueba_parser.has_relay_year():
                    inscription_data['event']['category'] = self.prueba_parser.relay_year
                else:
                    inscription_data['event']['category'] = match_competitor.group('year').strip()
            else:
                inscription_data['participant']['born_year'] = CompetitorParser.unify_born_year(int(match_competitor.group('year')), inscription_data['championship']['date'])
                CompetitorParser.update_leverade_data(match_competitor, inscription_data)
                CompetitorParser.update_category(inscription_data)
            self.prueba_parser.add_inscription_data(inscription_data)
            return True
        else:
            return False

    def get_inscription_data(self, match_competitor):
        group_dict = match_competitor.groupdict()
        return {
            'heat': self.prueba_parser.heat,
            'lane': 0,
            'session': self.prueba_parser.session,
            'club': match_competitor.group('club').upper().strip(),
            'initial_position': group_dict['inscription_data'] if 'inscripion_data' in group_dict else 999999
        }

    def get_championship_data(self, match_competitor):
        return {
            'name': self.championship_name.upper(),
            'date': self.championship_date,
            'pool': self.default_pool_length
        }

    def get_event_data(self, match_competitor):
        return {
            'event_number': self.prueba_parser.event_number,
            'distance': self.prueba_parser.distance,
            'style': self.prueba_parser.style,
            'gender': get_fixed_gender(self.prueba_parser.gender),
            'category': None,
            'type': self.prueba_parser.event_type
        }

    def get_participant_data(self, match_competitor):
        group_dict = match_competitor.groupdict()
        return {
            'license': None if 'license' not in group_dict else group_dict['license'].strip(),
            'name': group_dict['name'].strip().strip('*'),
            'born_year': None
        }

    def get_inscription_time(self, match_competitor):
        group_dict = match_competitor.groupdict()
        if 'inscription_time' in group_dict and group_dict['inscription_time'] is not None:
            time = group_dict['inscription_time'].lstrip('0').lstrip(':').strip()
            pool_length = self.default_pool_length
            chronometer_type = self.default_chronometer_type
        else:
            time = group_dict['time'].lstrip('0').lstrip(':').strip()
            pool_length = int(group_dict['pool_length']) if 'pool_length' in group_dict and group_dict['pool_length'] is not None else self.default_pool_length
            chronometer_type = group_dict['pool_type'] if 'pool_type' in group_dict and group_dict['pool_type'] else self.default_chronometer_type
        return {
            'time': self.process_inscription_time(time),
            'pool_length': pool_length,
            'chronometer_type': chronometer_type,
            'date': None,
            'location': None
        }

    def update_inscription_time_data(self, inscription_data, match_competitor):
        group_dict = match_competitor.groupdict()
        inscription_data['initial_position'] = int(match_competitor.group('initial_position')) if 'initial_position' in group_dict else 999999
        group_dict = match_competitor.groupdict()
        inscription_time_date = group_dict['inscription_time_date'] if 'inscription_time_date' in group_dict else None
        if inscription_time_date:
            inscription_data['inscription_time']['date'] = datetime.strptime(inscription_time_date.strip(), "%d/%m/%Y")
        else:
            inscription_data['inscription_time']['date'] = ''
        inscription_time_location = group_dict['inscription_time_location'] if 'inscription_time_location' in group_dict else None
        if inscription_time_location:
            inscription_data['inscription_time']['location'] = inscription_time_location.strip()
        else:
            inscription_data['inscription_time']['location'] = ''

    def update_inscription_time_data_from_db(self, inscription_data, collection):
        competitor_pre = collection.find_one(
            {
                'event.distance': inscription_data['event']['distance'],
                'event.style': inscription_data['event']['style'],
                'event.gender': get_fixed_gender(inscription_data['event']['gender']),
                'participant.licencse': inscription_data['participant']['license'],
                'championship.name': inscription_data['championship']['name']
            }
        )
        if competitor_pre:
            inscription_data['inscription_time']['date'] = competitor_pre['inscription']['time_date']
            inscription_data['inscription_time']['location'] = competitor_pre['inscription']['time_location']
            inscription_data['initial_position'] = competitor_pre['initial_position']

    def get_license_from_db(self, inscription_data, collection):
        competitor = collection.find_one(
            {
                'participant.name': inscription_data['participant']['name']
            }
        )
        return competitor['participant']['license']

    def set_prueba_parser(self, prueba_parser):
        self.prueba_parser = prueba_parser

    @staticmethod
    def process_inscription_time(inscription_time):
        if inscription_time == 'NT':
            inscription_time = '99:00.00'
        milliseconds_separator = '.' if '.' in inscription_time else ','
        if ':' in inscription_time:
            inscription_time_parts = inscription_time.split(':')
            minutes_time = int(inscription_time_parts[-2])
            seconds_time = int(inscription_time_parts[-1].split(milliseconds_separator)[0])
            milliseconds_time = int(inscription_time_parts[-1].split(milliseconds_separator)[1]) * 10
            if len(inscription_time_parts) > 2:
                minutes_time += int(inscription_time_parts[-3]) * 60
        else:
            if milliseconds_separator in inscription_time:
                minutes_time = 0
                seconds_time = inscription_time.split(milliseconds_separator)[0]
                milliseconds_time = inscription_time.split(milliseconds_separator)[1]
                if len(milliseconds_time) == 3:
                    seconds_time = seconds_time + milliseconds_time[0]
                    milliseconds_time = milliseconds_time[1:]
                seconds_time = int(seconds_time)
                milliseconds_time = int(milliseconds_time) * 10
            else:
                minutes_time = 0
                seconds_time = int(inscription_time) / 100
                milliseconds_time = int(inscription_time) % 100
        return timedelta(minutes=minutes_time, seconds=seconds_time,
                         milliseconds=milliseconds_time).total_seconds()

    @staticmethod
    def unify_born_year(born_year, competition_date):
        if born_year < 100:
            actual_year = competition_date.year % 100
            actual_century = 100 * int(competition_date.year / 100)
            if born_year > actual_year:
                born_year += actual_century - 100
            else:
                born_year += actual_century
        return born_year


    @staticmethod
    def get_category_from_born_year(born_year, competition_date):
        age = 5*int((competition_date.year - born_year)/5)
        return str(age)


class CompetitorParserBuilder:
    def __init__(self, parser_object=CompetitorParser('dummy')):
        self._parser_object = parser_object
        self._championship_name = 'a_champ'.upper()
        self._championship_date = datetime.strptime("16/11/2019", "%d/%m/%Y")
        self._default_pool_length = 25
        self._default_chronometer_type = 'E'

    def build(self):
        self._parser_object.championship_name = self._championship_name
        self._parser_object.championship_date = self._championship_date
        self._parser_object.default_pool_length = self._default_pool_length
        self._parser_object.default_chronometer_type = self._default_chronometer_type
        return self._parser_object

    def parser_object(self, parser_object):
        self._parser_object = parser_object
        return self

    def championship_name(self, championship_name):
        self._championship_name = championship_name
        return self

    def championship_date(self, championship_date):
        self._championship_date = championship_date
        return self

    def default_pool_length(self, default_pool_length):
        self._default_pool_length = default_pool_length
        return self

    def default_chronometer_type(self, default_chronometer_type):
        self._default_chronometer_type = default_chronometer_type
        return self


