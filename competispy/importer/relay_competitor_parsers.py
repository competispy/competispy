# -*- coding: UTF-8 -*-
import re
from competitor_parser import CompetitorParser, get_fixed_gender
from common_regex_strings import *


class RelevoParser(CompetitorParser):
    def __init__(self):
        super(RelevoParser, self).__init__(
            '\s*{}\s+{}\s+{}\s*{}\s*{}\s*{}\s+{}.{}'.format(initial_position_regex, license_regex, club_name_as_name_regex, year_regex, club_name_regex, inscription_time_regex, pool_length_regex, pool_type_regex))
        self.is_relay = True


class RelevoFinalParser(CompetitorParser):
    def __init__(self):
        super(RelevoFinalParser, self).__init__(
            '\s*{}\s+{}\s+{}\s*{}\s*{}\s*{}\s+{}.{}'.format(lane_regex, license_regex, club_name_as_name_regex, year_regex, club_name_regex, inscription_time_regex, pool_length_regex, pool_type_regex))
        self.is_relay = True
        self.use_update_from_db = True


class SplashMeetParserRelayCtoMadridWithHeat(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserRelayCtoMadridWithHeat, self).__init__(
            '\s*{}\s*{}?{}\s*{}\s*{}?{}\s*{}'.format(lane_regex, club_number_no_group_regex, club_name_as_name_regex, year_regex, club_number_regex, club_name_regex, inscription_time_regex))
        self.is_relay = True


class SplashMeetParserRelayCtoMadrid(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserRelayCtoMadrid, self).__init__(
            '\s*{}\s*{}?{}\s*{}\s*{}?{}\s*{}'.format(initial_position_regex, club_number_no_group_regex, club_name_as_name_regex, year_regex, club_number_regex, club_name_regex, inscription_time_regex))
        self.is_relay = True


class SplashMeetParserRelay(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserRelay, self).__init__(
            '\s*{}\s*{}\s*{}?\s*{}?{}\s*(({})|{})\s*{}?'.format(initial_position_regex, club_name_as_name_regex, year_regex, club_number_regex, club_name_regex, time_with_group_regex, no_time_regex, inscription_time_regex))
        self.is_relay = True


class SplashMeetParserRelayWithHeat(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserRelayWithHeat, self).__init__(
            '\s*{}\s*{}\s*{}\s*{}?{}\s*{}\s*{}'.format(lane_regex, club_name_as_name_regex, year_regex, club_number_regex, club_name_regex, inscription_time_regex, official_pool_length_regex))
        self.is_relay = True


class SplashMeetParserRelayCtoValencia(CompetitorParser):
    def __init__(self):
        super(SplashMeetParserRelayCtoValencia, self).__init__(
            '\s*{}\s+{}\s\d\s+{}\s*{}\s+{}?\s*{}'.format(initial_position_regex, club_name_as_name_regex, club_name_regex, inscription_time_no_group_regex, pool_length_regex, inscription_time_regex))
        self.is_relay = True
