# -*- coding: UTF-8 -*-
import re

from event_parser import PruebaParser
from competitor_parser import get_fixed_gender
from common_regex_strings import year_regex

heat_regex = '(?P<heat>\d+)'
event_number_regex = '(?P<event_number>\d+)'
distance_v1_regex = '(?P<distance>\S+)'
distance_v2_regex = '(?P<distance>\d\d+)'
style_regex = '(?P<style>\w+)'
gender_regex = '(?P<gender>\w+)'



class PruebaConSerieParser(PruebaParser):
    def __init__(self):
        super(PruebaConSerieParser, self).__init__()
        self.heat_re = re.compile('\s*((FINAL)|(SERIE))\s+{}'.format(heat_regex))

    def match(self, line):
        match_prueba_heat = self.heat_re.match(line)
        if match_prueba_heat:
            self.heat = int(match_prueba_heat.group('heat'))
            return False
        else:
            return self._match(line)

    def _match(self, line):
        raise NotImplemented


class PruebaNadadores(PruebaConSerieParser):
    def __init__(self):
        super(PruebaNadadores, self).__init__()
        self.regular_expression = re.compile(
            '\s*{}\s+(-\s+)?{}\s+m\.\s+{} {}'.format(event_number_regex, distance_v1_regex, style_regex, gender_regex))

    def _match(self, line):
        match_prueba = self.regular_expression.match(line)
        if match_prueba:
            self.event_number = int(match_prueba.group('event_number'))
            self.distance = int(match_prueba.group('distance').strip())
            self.style = match_prueba.group('style').upper().strip()
            self.gender = get_fixed_gender(match_prueba.group('gender').upper().strip())
            self.event_type = 'individual'
            print('match prueba de nadadores! event:{} {} {} {} {}'.format(self.event_number, self.distance, self.gender, self.event_type))
            return True
        return False


class PruebaRelevos(PruebaConSerieParser):
    def __init__(self):
        super(PruebaRelevos, self).__init__()
        self.regular_expression = re.compile(
            '\s*{}\s+((-\s)?)+4x{}\s+{} {}'.format(event_number_regex, distance_v1_regex, style_regex, gender_regex))

    def _match(self, line):
        match_prueba_relevo = self.regular_expression.match(line)
        if match_prueba_relevo:
            self.event_number = int(match_prueba_relevo.group('event_number'))
            self.distance = int(match_prueba_relevo.group('distance').strip())
            self.style = match_prueba_relevo.group('style').upper().strip()
            self.gender = get_fixed_gender(match_prueba_relevo.group('gender').upper().strip())
            self.event_type = 'relay'
            print('match prueba de relevos! event:{} {} {} {} {}'.format(self.event_number, self.distance, self.gender, self.event_type))
            return True
        return False


class SplashMeetEventRoot():
    def __init__(self, relay=False):
        if relay:
            relay_re = '(?P<is_relay>\d+ x )'
        else:
            relay_re = ''
        self.relay = relay
        self.event_re = '\s*Prueba {},?(\s+{}.?,)?\s+{}{}m\s+{}'.format(event_number_regex, gender_regex, relay_re, distance_v2_regex, style_regex)
        self.regular_expression = re.compile(self.event_re)

    def _match(self, line):
        match_prueba = self.regular_expression.match(line)
        if match_prueba:
            self.event_number = int(match_prueba.group('event_number'))
            self.distance = int(match_prueba.group('distance').strip())
            self.style = match_prueba.group('style').upper().strip()
            gender = match_prueba.group('gender')
            if gender is None:
                gender = "mixtoed"
            self.gender = get_fixed_gender(gender.upper().strip())
            if self.relay:
                self.event_type = 'relay'
            else:
                self.event_type = 'individual'
            print('match prueba {}! event:{} {} {} {}'.format('individual' if self.event_type == 'individual' else 'de relevos', self.event_number, self.distance, self.style, self.gender))
            return True
        return False


class SplashMeetEvent(PruebaParser, SplashMeetEventRoot):
    def __init__(self, relay=False):
        SplashMeetEventRoot.__init__(self, relay)
        PruebaParser.__init__(self)

    def match(self, line):
        return self._match(line)


class SplashMeetEventCtoValencia(PruebaParser, SplashMeetEventRoot):
    def __init__(self, relay=False):
        SplashMeetEventRoot.__init__(self, relay)
        PruebaParser.__init__(self)
        self.category_re = re.compile('{}'.format(year_regex))

    def match(self, line):
        match_category = self.category_re.match(line)
        if match_category:
            self.relay_year = match_category.group('year').strip()
            return False
        return self._match(line)


class SplashMeetEventSeries(PruebaParser, SplashMeetEventRoot):
    def __init__(self, relay=False):
        SplashMeetEventRoot.__init__(self, relay)
        PruebaParser.__init__(self)
        self.heat_re = re.compile('\s*Serie\s+{}\s+de\s+\d+'.format(heat_regex))

    def match(self, line):
        match_prueba_heat = self.heat_re.match(line)
        if match_prueba_heat:
            self.heat = int(match_prueba_heat.group('heat'))
            return False
        else:
            return self._match(line)

