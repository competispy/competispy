# -*- coding: UTF-8 -*-


class PruebaParser(object):
    def __init__(self):
        self.event_number = None
        self.distance = None
        self.style = None
        self.gender = None
        self.event_type = None
        self.heat = 0
        self.session = 0
        self.relay_year = None
        self.competitor_parsers = []
        self.competitors_data = []
        self.db = None

    def match(self, line):
        raise NotImplemented

    def add_competitor_parser(self, competitor_parser):
        competitor_parser.set_prueba_parser(self)
        self.competitor_parsers.append(competitor_parser)

    def add_inscription_data(self, inscription_data):
        self.competitors_data.append(inscription_data)

    def match_competitor(self, line):
        matched = False
        for competitor_parser in self.competitor_parsers:
            matched = competitor_parser.match(line) or matched
        return matched

    def set_db(self, db):
        self.db = db

    def has_relay_year(self):
        return self.relay_year is not None

    def store(self):
        if self.db is not None:
            for competitor in self.competitors_data:
                c = {'championship': competitor['championship'], 'event': competitor['event'],
                     'participant': competitor['participant']}
                existent_competitor = self.db.find_one(c)
                if existent_competitor:
                    print('UPDATE: ' + str(competitor))
                    self.db.update_one(existent_competitor,
                                       {'$set': {
                                           'heat': competitor['heat'],
                                           'lane': competitor['lane'],
                                           'session': competitor['session']
                                       }})
                else:
                    print('NEW ENTRY: ' + str(competitor))
                    self.db.insert_one(competitor)
