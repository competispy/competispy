import requests
import pymongo
import os


class TestingDb:
    def insert(self, data):
        print('inserting:', data)
        print()

    def remove(self, key):
        pass

    def find_one(self, key):
        return None


class RemoteRestDb:
    def __init__(self, server_url='http://localhost:8080'):
        self.server_url = server_url

    def insert(self, data):
        output_data = data.copy()
        output_data['inscription_time']['date'] = str(output_data['inscription_time']['date'])
        output_data['championship']['date'] = str(output_data['championship']['date'])
        output_data['user'] = os.environ['COMPETISPY_ADMIN_USER']
        output_data['pass'] = os.environ['COMPETISPY_ADMIN_PASS']
        r = requests.post('%s/inscriptions' % self.server_url, json=output_data)
        print('response code:', r.status_code, 'response:', r.text)
        return

    def remove(self, key):
        pass

    def find_one(self, key):
        return None


class TestAdaptor:
    def __init__(self):
        self.db = TestingDb()

    def get_collection(self, db, collection):
        return self.db


class RemoteRestAdaptor:
    def __init__(self, server_url='http://localhost:8080'):
        self.db = RemoteRestDb(server_url)

    def get_collection(self, db, collection):
        return self.db


class MongoAdaptor:
    def __init__(self, host, mongo_port, username, password, auth_source="admin", auth_mechanism='SCRAM-SHA-256'):
        self.host = host
        self.port = mongo_port
        self.user = username
        self.password = password
        self.auth_source = auth_source
        self.auth_mechanism = auth_mechanism
        self.mongo_uri = self.host + ':' + self.port
        self.mongo_client = pymongo.MongoClient(self.mongo_uri, username=self.user, password=self.password,
                                                authSource=self.auth_source, authMechanism=self.auth_mechanism,
                                                connect=False)

    def get_collection(self, db, collection):
        return self.mongo_client[db][collection]
