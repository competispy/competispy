import os

from competispy.db.DatabaseConnector import TestAdaptor, RemoteRestAdaptor, MongoAdaptor

mongo_host = 'db-svc'
mongo_port = '27017'
mongo_user = os.environ['COMPETISPY_MONGO_APPLICATION_USER']
mongo_pass = os.environ['COMPETISPY_MONGO_APPLICATION_PASS']


class DbAdaptor:
    def __init__(self):
        self.db_client = None
        self.test = False
        self.mode = 'local'

    def setup(self, mode='local', test=False, host=mongo_host):
        if self.db_client is not None:
            return
        self.mode = mode
        self.test = test
        if self.mode == 'local':
            if self.test:
                print('test mode')
                self.db_client = TestAdaptor()
            else:
                print('db mode')
                self.db_client = MongoAdaptor(host, mongo_port, username=mongo_user, password=mongo_pass,
                                              auth_source="admin", auth_mechanism='SCRAM-SHA-256')
        elif mode == 'remote_rest':
            self.db_client = RemoteRestAdaptor()

    def get_inscriptions_db(self):
        return self.db_client.get_collection('competispy', 'inscriptions')

    def get_times_db(self):
        return self.db_client.get_collection('competispy', 'times')

    def get_user_config_db(self):
        return self.db_client.get_collection('competispy', 'userconfig')


db_adaptor = DbAdaptor()
