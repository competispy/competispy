# competispy

## First start

Create the file **.env** with the following content
```
COMPETISPY_ADMIN_USER=[admin_user]
COMPETISPY_ADMIN_PASS=[admin_pass]
```
Execute:
```
docker-compose up -d
```

The database needs to start and get configured, so it is normal that the first access to the app ends in a 500 server error due to no access to the DB

Give some time to the app to finish the initialization, stop docker and start it again
```
docker-compose down
docker-compose up -d
```

## Adding competitions

The DB will not have any competition.

To add a competition:

```bash
cd importer
python3 importer.py
```
Now the DB will have a competition and searches will be possible

## App structure

The app is done with the Flask-RESTFul framework, but it is still working by rendering mako HTML templates. The templates can be found at **app/templates/*.mako**

The REST interface is usable but the frontend still needs to be refactored to use it.

The **importer** folder contains the scripts to uplad new entries to the DB. The importer can be configured to import data with the REST API or directly to the DB if executed in an environment with direct access to the DB.

The entries are stored in a MongoDB.

Each entry contains all the needed info of an individual inscription to the competition. This is, participant name, surname, inscription time, competition...

## Improvements

 - Importer
 - The app bakcend itself needs refactoring
 - The app frontend can be replaced by one that uses the REST API
 - Review the security of the app administration user that is enabled to add entries by calling the REST API
 - The DB usage can be improved
 - The DB keys can be secured by using environment variables
---------

# REST API

Two endpoints exist:

 - /championships
 - /inscriptions

## Championships endpoint
### GET
Returns a list with the competition names

## Inscriptions endpoint
### GET
Returns a list with all the inscriptions that match the filter.

The **GET** request must include a filter in the form of URL parameters.

The list of the available filter parameters is:
 - **'championship_name'**: **Required**, type=string. The value must be one of the returned by the *championships* endpoint
 - 'event_distance': type=number. Limits the returned inscriptions to the ones of the given distance. For instance, 50, 100, 200... 
 - 'event_style': type=string. Limits the returned inscriptions to the ones of the given style. For instance, libre, espalda, braza, estilos...
 - 'event_gender': type=string. It can be *MASCULINO* or *FEMENINO*.
 - 'event_type': type=string. It can be *individual* or *relay*.
 - 'event_category': type=string. Limits the results to the one that belong to the given category. For instance, 30, 35, 40... for individual events or +100, +120, +160... for relay events.
 - 'participant_name': type=string. Filters by the participant name. This field works as a regular expression
 - 'participant_license': type=string. Filters by the participant license number
 - 'participant_born_year_gte': type=string. If given the result will only contain inscriptions on which the participant born year is greater or equal (this is younger than) to the given value
 - 'participant_born_year_lt': type=string. If given the result will only contain inscriptions on which the participant born year is below (this is older than) the given value
 - 'participant_leverade_name': type=string. This field works as a regular expression
 - 'participant_leverade_surname': type=string. This field works as a regular expression
 - 'heat': type=number,
 - 'lane': type=number,
 - 'session': type=number,
 - 'club': type=string. Limits the returned inscriptions to the ones of the given club name. This field works as a regular expression
 - 'inscription_pool_length': type=number. Limits the results to the ones which inscription time belongs to a pool length of the given value. It can take the values *25* or *50*
 - 'inscription_chronometer_type': type=string. It can be *E* to only return inscriptions which inscription time was taken with an electronic chronometer or *M* if it was done with a manual chronometer
 - 'start_index': type=number, default=0. Skips a number of results given by the value
 - 'sort_mode': type=string, default='inscription_time'. Sorts the results by the given attribute
 - 'results_limit': type=number, default=10, range=[1,100]. Limits the number of returned inscriptions to the given value

The response data is an array of elements similar to the following ones:
```
{'championship': {'date': '01/06/2019',
                    'name': 'CHAMPIONSHIP NAME'},
  'club': 'CLUB NAME',
  'event': {'category': '25',
             'distance': 400,
             'event_number': 26,
             'gender': 'MASCULINO',
             'style': 'ESTILOS',
             'type': 'individual'},
  'heat': 0,
  'initial_position': 3,
  'inscription_time': {'chronometer_type': 'E',
                        'date': '',                      # It may be empty
                        'location': '',                  # It may be empty
                        'pool_length': 50,
                        'time': 330.15},                 # Reflects the time in a pool of the given length
  'lane': 0,
  'participant': {'born_year': 1950,
                   'leverade': {'name': 'Andy',          # It may not be present
                                 'surname': 'LUCAS'},    # It may not be present
                   'license': None,
                   'name': 'LUCAS, Andy'},
  'session': 0}
`````````

or

```
{'championship': {'date': '02/06/2018',
                    'name': 'A CHAMPIONSHIP NAME'},
  'club': 'CLUB NAME',
  'event': {'category': '+120',
             'distance': 50,
             'event_number': 11,
             'gender': 'MASCULINO',
             'style': 'LIBRE',
             'type': 'relay'},
  'heat': 5,
  'initial_position': 999999,
  'inscription_time': {'chronometer_type': 'E',
                        'date': '02/06/2018',
                        'location': 'unknown',
                        'pool_length': 25,
                        'time': 100.92},
  'lane': 4,
  'participant': {'born_year': 120,
                   'license': 'license string',
                   'name': 'PARTICIPANT NAME'},
  'session': 0}

```

