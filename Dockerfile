# Generate workable requirements.txt from Poetry dependencies
FROM python:3.12-alpine as requirements

RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev && pip install --upgrade pip
RUN python -m pip install --no-cache-dir --upgrade poetry

WORKDIR /src
COPY pyproject.toml poetry.lock ./
RUN poetry self add poetry-plugin-export && poetry export -f requirements.txt --without-hashes -o /src/requirements.txt

# Final app image
FROM python:3.12-alpine as backend

RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev && pip install --upgrade pip

# Switching to non-root user appuser
RUN addgroup -S appuser && adduser -S appuser -G appuser
WORKDIR /home/appuser
USER appuser:appuser
ENV PATH="$PATH:/home/appuser/.local/bin"

# Install requirements
COPY --from=requirements /src/requirements.txt .
RUN pip install --no-cache-dir --user -r requirements.txt

EXPOSE 8080

COPY competispy ./competispy
CMD ["gunicorn", "-c", "competispy/gunicorn_conf.py", "competispy:create_app()"]
